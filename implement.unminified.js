/**
 * Implement iPlugins.
 */

(function (window, document) {
    var implementationTag = document.getElementById('iplugins-implementation-tag');
    var async = (document.currentScript && document.currentScript.async) || implementationTag && implementationTag.hasAttribute('async');
    var loadVersion = implementationTag ? implementationTag.getAttribute('data-version') || '' : '';
    var version = localStorage.getItem('iv') || '2.0.5';

    var load = function (url) {
        (function (tag, url, scriptTag) {
            scriptTag = document.createElement(tag);
            scriptTag.async = async;
            scriptTag.src = url;

            if (async) {
                var bodyElem = document.head;
                if (bodyElem) {
                    bodyElem.appendChild(scriptTag);
                }
            } else {
                document.write(scriptTag.outerHTML);
            }
        })('script', url);
    };

    // Load 2.0
    if (loadVersion === '' || loadVersion === '2') {
        (function (url, name) {
            // add a queue event here
            window['_iPluginsReference'] = name;
            window[name] = window[name] || function () {
                (window[name].q = window[name].q || []).push(arguments);
            };
            window[name].mapper = window[name].mapper || {
                'ar_SA': 'en_SA',
                'ar_BH': 'en_BH',
                'ar_MA': 'en_MA',
                'fr_MA': 'en_MA',
                'ar_MA-m': 'en_MA-m',
                'fr_MA-m': 'en_MA-m',
                'ar_KW': 'en_KW',
                'ar_JO': 'en_JO',
                'fr_CA': 'en_CA',
                'fr_CA-m': 'en_CA-m',
                'ms_MY': 'en_MY',
                'th_TH': 'en_TH',
                'ar_QA': 'en_QA',
                'ar_AE': 'en_AE',
                'ar_EG': 'en_EG',
                'en_PT': 'pt_PT',
                'en_PT-m': 'pt_PT-m',
                'en_PT-m2': 'pt_PT-m2',
                'fr_BE': 'nl_BE'
            };
            load(url);
        })('//ww8.ikea.com/ext/iplugins/releases/iplugins.' + version + '.min.js', 'iPlugins');
    }

})(window, document);