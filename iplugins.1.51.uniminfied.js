/**
 * Implement iPlugins.
 * implement.js version 1.33
 *
 * Setings from iplugins implement tag code:
 * window.ipluginsImplementationSettings.hideTime => time in milliseconds to hide the body (Currently not used)
 */

(function (window, document) {
    // Get iPlugins account.
    var iPluginsAccount = getIpluginsAccount();

    /* Quick and dirty fix for Swedish checkout - START */
    /* Remove after swithhing to OneCheckout or our own */
    if (iPluginsAccount == 'sv_SE') {
        if (document.location.href.indexOf('/webapp/wcs/stores/servlet/OrderItemDisplay') > -1 || document.location.href.indexOf('/webapp/wcs/stores/servlet/IrwOrderItemDisplay') > -1 || document.location.href.indexOf('/webapp/wcs/stores/servlet/PromotionCodeManage') > -1) {
            document.addEventListener("DOMContentLoaded", function() {
                setInterval(function() {
                    document.cookie = "preferedui=; path=/; domain=.ikea.com; expires=Thu, 01 Jan 1970 00:00:01 GMT";
                }, 300);
            });
        }
    }
    /* Quick and dirty fix for Swedish checkout - END */

    // Load iPlugins 1.51
    // ------------------
    var baseProductionUrl;

    if ((document.URL.indexOf('noiplugins') == -1) // Disable iPlugins
        && (document.URL.indexOf('disableiplugins151') == -1) // Disable only 1.51
        && (document.URL.indexOf('ww8.ikea.com/checkout/') == -1)) { // Do not run 1.51 on the Klarna checkout pages (Sweden fix)

        // Run for all browsers except for IE10 and bellow.
        var ieVersion = getIEVersion();
        if ((ieVersion == false) || (ieVersion && (ieVersion > 10))) {

            // Check if account is set.
            if (iPluginsAccount) {

                var pageUrl = document.URL,
                    baseProductionUrl = 'https://ww8.ikea.com/ext/iplugins/' + iPluginsAccount + '/production/',
                    baseLibsUrl = 'https://ww8.ikea.com/ext/iplugins/' + iPluginsAccount + '/resources/libs/';
                //baseProductionUrl = '//localhost/ikea/iplugins/accounts/' + iPluginsAccount + '/production/';

                // Do not load custom libs on Click and Collect pages.
                if (document.URL.indexOf('ww8.ikea.com/clickandcollect') == -1) {

                    // Load libs only for Sweden.
                    // (Possibility to load libs for each market separately will be implemneted later in iPlugins.)
                    if (iPluginsAccount == 'sv_SE') {
                        loadFilesAsynchronously([
                            //baseLibsUrl + 'js/jquery-3.2.1.min.js',
                            baseLibsUrl + 'js/handlebars-v4.0.6.min.js',
                            baseLibsUrl + 'js/apptus-esales-api-1.0.2.min.js'
                        ], function () { }, this, false);
                    }
                }

                // If user starts the test in the iplugins console,
                // set test cookie and then implement test.
                if (pageUrl.indexOf('iplugins_sc') > -1) {
                    var testKey = getQueryStringValue('iplugins_sc');
                    if (testKey && !/^\d+$/.test(testKey)) {
                        setTestCookie(testKey, function () {
                            implementTest(testKey);
                        });
                    }

                    // When user clicks on the "Exit test" button in the test companion,
                    // remove test cookie and reload the page.
                } else if (pageUrl.indexOf('iplugins_rc') > -1) {
                    removeTestCookie();

                    // If there is no iplugins query string,
                    // run implementation.
                } else {

                    // Get test key from the query string or cookie.
                    // Query string has an advantage.
                    if (pageUrl.indexOf('iplugins_qs') > -1) {
                        testKey = getQueryStringValue('iplugins_qs');
                    } else {
                        testKey = getCookieValue('iplugins_c');
                    }

                    // If test key exists, implement test,
                    // otherwise, implement production.
                    if (testKey && !/^\d+$/.test(testKey)) {
                        implementTest(testKey);
                    } else {
                        implementProduction();
                    }
                }
            }
        }
    }

    /**
     * Implement test files.
     *
     * @param string testKey
     * @return void
     */
    function implementTest(testKey) {
        var ajaxData = {},
            typeDirName = '',
            testName = '',
            testType = testKey.substring(0, 1),
            settings;

        ajaxData.response = 'text',
            ajaxData.async = false;

        if (testType == 'd') {
            testName = 'Data';
            ajaxData.url = baseProductionUrl + 'data/test-settings.json';
        } else if (testType == 'e') {
            typeDirName = 'editions';
            testName = 'Edition';
            ajaxData.url = baseProductionUrl + 'editions/test-settings.json';
        } else if (testType == 'p') {
            typeDirName = 'plugin-tests';
            testName = 'Plugins';
            ajaxData.url = baseProductionUrl + 'plugin-tests/test-settings.json';
        }

        // Get settings from the json file.
        runAjaxRequest(ajaxData, function (allSettingsJson) {
            if (allSettingsJson) {
                //console.log(allSettingsJson);
                var isTestDisabled = true,
                    allSettings = JSON.parse(allSettingsJson);

                for (var i = 0; i < allSettings.length; i++) {
                    if (allSettings[i].testKey == testKey) {
                        isTestDisabled = false;
                        settings = allSettings[i];

                        // When testing data, only a cookie with data directory will be set.
                        if (testType == 'd') {
                            document.cookie = 'iplugins_pdd=' + settings.dirName + '; domain=.ikea.com; path=/';

                            // Load test files when testing plugins/editions.
                        } else if ((testType == 'p') || (testType == 'e')) {
                            runLoad(typeDirName, settings);
                        }

                        // Show test info in the test companion in the top left corner of the page.
                        showTestCompanion(testName, settings.dirName);
                    }
                }

                // If test cookie exists but if test is disabled in iplugins,
                // remove test cokie and reload page.
                if (isTestDisabled) removeTestCookie();
            }
        });
    }

    /**
     * Implement production files.
     *
     * @return void
     */
    function implementProduction() {
        var ajaxData = {};

        ajaxData.response = 'text',
            ajaxData.async = false,
            ajaxData.url = baseProductionUrl + 'production-settings.json';

        // Get production settings from the json file.
        runAjaxRequest(ajaxData, function (settingsJson) {
            if (settingsJson) {
                //console.log(settingsJson);
                var settings = JSON.parse(settingsJson);

                // If production is set to Online in the iplugins console.
                if (settings.productionStatus == 'online') {
                    runLoad('editions', settings);
                }
            }
        });
    }

    /**
     * Run load files.
     *
     * @param object typeDirName (editions/plugin-tests)
     * @param object settings
     * @return void
     */
    function runLoad(typeDirName, settings) {

        // Load prio files.
        var prioFilesToLoad = prepareFilesForLoad('prio', typeDirName, settings);
        if (prioFilesToLoad && prioFilesToLoad.length > 0) {

            // If m or m2, run even prio files asynchronously.
            if (iPluginsAccount.indexOf('-m') > -1) loadFilesAsynchronously(prioFilesToLoad, function () { }, this, true);
            else loadFilesSynchronously(prioFilesToLoad);
        }

        // IMPORTANT.
        // iPlugins should not send devUrls with empty items. Remove this fix when it is fixed in iPlugins and use bellow one.
        // --- Start ---
        // Load files under development.
        var siteType = 'desktop';
        if (settings.devUrls && settings.devUrls[siteType]) {
            var filteredDevUrls = settings.devUrls[siteType].filter(function (element) {
                if (element != '') return element;
            });

            if (filteredDevUrls != 0) {

                // If m or m2, run even prio files asynchronously.
                if (iPluginsAccount.indexOf('-m') > -1) loadFilesAsynchronously(settings.devUrls[siteType], function () { }, this, true);
                else loadFilesSynchronously(settings.devUrls[siteType]);
            }
        }
        // --- End ---

        // Load standard.js asynchronously when ipluginsCommon (merged in prio.js) is ready.
        // Because, if standard.js should load faster then prio.js, then ipluginsCommon used in standard.js would be undefined.
        //
        // If variable settings.existsCommonJs exists and is false, just load standard files.
        // And if not, first find ipluginsCommon and then load standard. This will happen in two cases:
        // 1. settings.existsCommonJs do not exists (all test cases and editions created before introducing this variable).
        // 2. settings.existsCommonJs exists and is true.
        if ((settings.existsCommonJs === 'undefined') || (settings.existsCommonJs !== 'undefined') && (settings.existsCommonJs == false)) {
            loadStandardFiles();
        } else {
            var findIpluginsCommon = setInterval(function () {
                if (window.ipluginsCommon) {

                    // Load standard files.
                    loadStandardFiles();
                    clearInterval(findIpluginsCommon);
                }

                // Do not load standard.js if ipluginsCommon is not found 2 seconds after prio.js files is loaded.
                window.setTimeout(function () {
                    clearInterval(findIpluginsCommon);
                }, 2000);
            }, 50);
        }

        function loadStandardFiles() {
            var standardFilesToLoad = prepareFilesForLoad('standard', typeDirName, settings);
            if (standardFilesToLoad && standardFilesToLoad.length > 0) {
                loadFilesAsynchronously(standardFilesToLoad, function () { }, this, true);
            }
        }
    }

    /**
     * Prepare files for load.
     * Buld urls to the files for load.
     *
     * @param string loadType    (standard/prio)
     * @param object typeDirName (editions/plugin-tests)
     * @param object settings
     * @return void
     */
    function prepareFilesForLoad(loadType, typeDirName, settings) {
        var filesToLoad = [],
            siteType = 'desktop',
            dirUrl = baseProductionUrl + typeDirName + '/' + settings.dirName,
            fileUrlWithoutExtension = dirUrl + '/' + loadType;

        // Use minimized/unminimized files for different types of loads and purposies.
        if (typeDirName == 'editions') fileUrlWithoutExtension += '.min';

        // Create file names for css and js files,
        // to be able to check in the test-settings what files to load.
        var cssFileName = siteType + loadType.charAt(0).toUpperCase() + loadType.slice(1) + 'Css';
        var jsFileName = siteType + loadType.charAt(0).toUpperCase() + loadType.slice(1) + 'Js';

        // Create a list of files to load.
        if (settings.files) {
            if (settings.files[cssFileName]) filesToLoad.push(fileUrlWithoutExtension + '.css');
            if (settings.files[jsFileName]) filesToLoad.push(fileUrlWithoutExtension + '.js');
        }
        return filesToLoad;
    }

    /**
     * Set test cookie.
     *
     * @param string   testKey
     * @param function callback
     * @return void
     */
    function setTestCookie(testKey, callback) {
        var domain = '.ikea.com';

        // Exception for China.
        if (document.URL.indexOf('ikea.cn') > -1) domain = '.ikea.cn';

        document.cookie = 'iplugins_c=' + testKey + '; domain=' + domain + '; path=/';
        callback();
    }

    /**
     * Remove test cookie and reload page.
     *
     * @return void
     */
    function removeTestCookie() {
        document.cookie = 'iplugins_c=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=.ikea.com; path=/';
        window.location = document.URL.split('?')[0];
    }

    /**
     * Check if our query string is present in the current url.
     *
     * @param string name
     * @return string
     */
    function getQueryStringValue(name) {
        name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
        var regexS = '[\\?&]' + name.toLowerCase() + '=([^&#]*)';
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.search.toLowerCase());
        if (results == null) {
            return '';
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, ' '));
        }
    }

    /**
     * Get cookie value.
     *
     * @param string cookieName
     * @return string
     */
    function getCookieValue(cookieName) {
        var match = document.cookie.match(new RegExp(cookieName + '=([^;]+)'));
        if (match) {
            return match[1];
        }
    }

    /**
     * Run AJAX request.
     *
     * @param object parameters
     *    string method
     *    string url
     *    boolean async
     *    string response
     * @param function callback
     * @return void
     */
    function runAjaxRequest(parameters, callback) {

        // Set default values.
        if (parameters.method == undefined) parameters.method = 'get';
        if (parameters.async == undefined) parameters.async = true;
        if (parameters.response == undefined) parameters.response = 'xml';

        // Create the object for browsers.
        var xmlHttp = new XMLHttpRequest();

        xmlHttp.onreadystatechange = function () {

            // If server is ready with the response.
            if (xmlHttp.readyState == 4) {

                // If everything is Ok on browser.
                if (xmlHttp.status == 200) {
                    var response = xmlHttp.responseXML;
                    if (parameters.response == 'text') response = xmlHttp.responseText;
                    callback(response);
                } else {
                    callback(0);
                }
            }
        }
        xmlHttp.open(parameters.method, parameters.url, parameters.async);
        xmlHttp.send();
        //}
    }

    /**
     * Loads a given set of files (.css/.js) synchronously.
     *
     * @param array fileList
     * @return void
     */
    function loadFilesSynchronously(fileList) {
        if (fileList) {
            var nrOfFilesToLoad = fileList.length;
            for (var i = 0; i < nrOfFilesToLoad; i++) {
                var extension = fileList[i].split('.').pop();

                if (extension == 'css') {
                    document.write('<link rel="stylesheet" type="text/css" href="' + fileList[i] + '" />');
                } else if (extension == 'js') {
                    document.write('<script type="text/javascript" src="' + fileList[i] + '"></script>');
                }
            }
        }
    }

    /**
     * Loads a given set of (.css/.js) files.
     * Calls the callback function when all files have been loaded.
     * Set preserveOrder to true to ensure non-parallel loading of files if load order is important.
     *
     * @param {Array}    fileList      Array of all files to load
     * @param {Function} callback      Callback to call after all files have been loaded
     * @param {Object}   scope         The scope to call the callback in
     * @param {Boolean}  preserveOrder True to make files load in serial, one after the other (defaults to false)
     * @return void
     */
    function loadFilesAsynchronously(fileList, callback, scope, preserveOrder) {
        var scope = scope || this,
            head = document.getElementsByTagName("head")[0],
            fragment = document.createDocumentFragment(),
            numFiles = fileList.length,
            loadedFiles = 0;

        /**
         * Loads a particular file from the fileList by index.
         * This is used when preserving order.
         */
        var loadFileIndex = function (index) {
            head.appendChild(
                buildScriptTag(fileList[index], onFileLoaded)
            );
        };

        /**
        * Callback function which is called after each file has been loaded.
        * This calls the callback passed to load once the final file in the fileList has been loaded.
        */
        var onFileLoaded = function () {
            loadedFiles++;

            // If this was the last file, call the callback, otherwise load the next file.
            if (numFiles == loadedFiles && typeof callback == 'function') {
                callback.call(scope);
            } else {
                if (preserveOrder === true) {
                    loadFileIndex(loadedFiles);
                }
            }
        };

        // Load files preserving order.
        if (preserveOrder === true) {
            loadFileIndex.call(this, 0);

            // Load each file (most browsers will do this in parallel).
        } else {
            for (var index = 0; index < numFiles; index++) {
                fragment.appendChild(
                    buildScriptTag(fileList[index], onFileLoaded)
                );
            }
            head.appendChild(fragment);
        }
    }

    /**
     * Creates and returns a script tag, but does not place it into the document.
     * If a callback function is passed, this is called when the script has been loaded.
     *
     * @param {String}   filename The name of the file to create a script tag for
     * @param {Function} callback Optional callback, which is called when the script has been loaded
     * @return {Element} The new script tag
     */
    function buildScriptTag(filename, callback) {
        var exten = filename.substr(filename.lastIndexOf('.') + 1),
            elem;

        //console.log('Loader.buildScriptTag: filename=[%s], exten=[%s]', filename, exten);

        if (exten == 'js') {
            var script = document.createElement('script');
            script.type = "text/javascript";
            script.src = filename;

            elem = script;

        } else if (exten == 'css') {
            var style = document.createElement('link');
            style.rel = 'stylesheet';
            style.type = 'text/css';
            style.href = filename;

            elem = style;
        }

        // Check if element is loaded.
        if (elem) {
            if (elem.readyState) {
                elem.onreadystatechange = function () {
                    if (elem.readyState == "loaded" || elem.readyState == "complete") {
                        elem.onreadystatechange = null;
                        callback();
                    }
                };
            } else {
                elem.onload = callback;
            }
        }

        return elem;
    }

    /**
     * Get IE version.
     * If browser is IE, return IE version, otherwise return "false".
     *
     * @return string/boolean
     */
    function getIEVersion() {
        var ua = window.navigator.userAgent,
            msie = ua.indexOf('MSIE ');

        if (msie > 0) {
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        } else {
            return false;
        }
    }

    /**
     * Get iPlugins account.
     *
     * @return string
     */
    function getIpluginsAccount() {
        var account;

        // First try to get account name from the lang attribute
        var html = document.documentElement;
        var langValue = html.lang || html.getAttribute('xml:lang') || false;
        if (langValue && langValue.length == 5) { // Some "lang" attributes contains only language ex. "se". Ignore those.
            var regExpMobileVersion = /^(m2?)/i;
            var mobileVersion = regExpMobileVersion.exec(location.hostname);

            account = langValue.replace('-', '_') + (mobileVersion ? '-' + mobileVersion[1] : '');
        }

        // If no account identified, try to get account from the URL
        if (!account) {
            if ((document.URL.indexOf('https://www.ikea.com/') > -1) &&
                (document.URL.indexOf('webapp/wcs/stores/servlet/mOrderItemDisplayView') == -1) &&   // No lang and country code present in this URL
                (document.URL.indexOf('webapp/wcs/stores/servlet/OrderItemDisplayMobile') == -1)) {  // No lang and country code present in this URL

                var values = location.pathname.split('/');
                if (document.URL.indexOf('www.ikea.com/ms/') > -1) {
                    account = values[2];
                } else if (document.URL.indexOf('www.ikea.com/ext/local-store/') > -1) {
                    account = values[4] + '_' + values[3].toUpperCase();
                } else if (document.URL.indexOf('mcommerce/shoppingcart') > -1) {
                    account = values[2] + '_' + values[1].toUpperCase() + '-m';
                } else {
                    account = values[2] + '_' + values[1].toUpperCase();
                }
            }
        }

        // If no account identified, get it from the data-account attribute.
        //
        // This checks lastly because we cannot trust that right iPlugins tag will be implemented by Tealium.
        // IRW sends wrong account to Telium that then inserts wrong iPlugins account.
        // This causes that content from countries can be mixed up.)
        if (!account) {
            var implementationTag = document.getElementById('iplugins-implementation-tag');
            if (implementationTag) {
                var iPluginsAccountFromTag = implementationTag.getAttribute('data-account');
                if (iPluginsAccountFromTag) account = iPluginsAccountFromTag;
            }
        }

        // If no account identified, check if window variable ipluginsImplementationSettings exists (old method).
        // Some countries use this, ex. Sweden.
        if (!account) {
            if ((window.ipluginsImplementationSettings) && (window.ipluginsImplementationSettings.account)) {
                account = window.ipluginsImplementationSettings.account;
            }
        }

        // Exceptions
        // ----------

        // Exception for China
        if ((account == 'en_CN') || (account == 'en_CN-m2') || (account == 'zh_CN') || (account == 'zh_CN-m2')) {
            if ((document.URL.indexOf('internal.cmirw101.cm01.test.ikea') > -1) ||
                (document.URL.indexOf('internal.cfirw101.cf03.test.ikea') > -1) ||
                (document.URL.indexOf('internal.pirw401.ikea.cn') > -1)) {
                account += '-test';
            }
        }

        // Exceptions for ROIG
        if (account == 'ar_SA') account = 'en_SA';
        else if (account == 'ar_SA-m') account = 'en_SA-m';
        else if (account == 'ar_KW') account = 'en_KW';
        else if (account == 'ar_KW-m') account = 'en_KW-m';
        else if (account == 'ar_JO') account = 'en_JO';
        else if (account == 'ar_JO-m') account = 'en_JO-m';
        else if (account == 'fr_MA') account = 'en_MA';
        else if (account == 'ar_MA') account = 'en_MA';
        else if (account == 'ar_QA') account = 'en_QA';
        else if (account == 'ar_EG') account = 'en_EG';

        // Exceptions for Spain
        else if (account == 'ca_ES') account = 'es_ES';
        else if (account == 'eu_ES') account = 'es_ES';

        // Exceptions for Portugal
        else if (account == 'en_PT') account = 'pt_PT';
        else if (account == 'en_PT-m') account = 'pt_PT-m';

        // Exception for Canada.
        else if (account == 'fr_CA') account = 'en_CA';
        else if (account == 'fr_CA-m') account = 'en_CA-m';

        // Fix for Norway.
        if (account && account.indexOf('nb_') > -1) account = account.replace('nb_', 'no_');
        if (account == 'no_NO-m') account = 'no_NO-m2';

        // Exception for my/th markets.
        if (account == 'ms_MY') account = 'en_MY';
        else if (account == 'th_TH') account = 'en_TH';
        else if (account == 'ms_MY-m') account = 'en_MY-m';
        else if (account == 'th_TH-m') account = 'en_TH-m';

        // Return
        if (account) {
            // Temp fix for Norway.
            if (account.indexOf('nb') > -1) {
                account = iPluginsAccount.replace('nb', 'no');
            }

            return account;
        } else {
            return false;
        }
    }

    /**
     * Show test info in a test companion in the the upper left corner.
     *
     * @param string testType
     * @param string testName
     * @return void
     */
    function showTestCompanion(testType, testName) {
        var testCompanionHolder = document.createElement("div");
        testCompanionHolder.setAttribute("style", "position: fixed; top: 10px; left: 10px; margin: 0px; padding: 7px; border-radius: 5px; border: 1px solid rgb(217, 217, 217); font-family: Verdana; font-size: 12px; text-align: left; z-index: 10000; background-color: rgb(242, 242, 242);");
        var testCompanionContent = '<span style="display: block; color: rgb(51, 153, 255); font-weight: bold; font-size: 14px;">iplugins</span><span style="display: block; margin-bottom: 7px; color: rgb(51, 153, 255); font-size: 12px;">test mode</span><span style="display: block; margin-bottom: 2px; color: rgb(179, 179, 179); font-style: italic;">Test type</span><span style="display: block; margin-bottom: 7px;">' + testType + '</span><span style="display: block; margin-bottom: 2px; color: rgb(179, 179, 179); font-style: italic;">Test name</span><span style="display: block;">' + testName + '</span><a href="#" onclick="window.open(document.URL + ((document.URL.indexOf(\'?\') > -1) ? \'&\' : \'?\') + \'iplugins_rc\', \'_self\');" style="display: block; width: 100%; background-color: #0099ff; text-align: center; padding: 5px 0px 5px 0px; margin-top: 10px; color: white; border-radius: 4px; text-decoration: none; transition: .2s background-color;" onMouseOver="this.style.backgroundColor=\'#4db8ff\'" onMouseOut="this.style.backgroundColor=\'#0099ff\'">Exit test</a>';
        testCompanionHolder.innerHTML = testCompanionContent;

        var mainHolder = document.getElementsByTagName('html').item(0);
        mainHolder.appendChild(testCompanionHolder);
    }
})(window, document);